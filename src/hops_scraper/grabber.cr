class HoPSScraper::Grabber
  CLIENT_COUNT = 16
  @clients : Channel(HTTP::Client)

  @lecturers : Array(Lecturer)? = nil
  @degree_courses : Array(DegreeCourse)? = nil
  @timetable_entries : Array(TimetableEntry)? = nil
  @blocktable_entries : Array(BlocktableEntry)? = nil

  getter lectures : Array(Lecture) { self.fetch_lectures }
  getter modules : Array(Module) { self.fetch_modules }

  def initialize
    @clients = Channel(HTTP::Client).new(CLIENT_COUNT)
    CLIENT_COUNT.times do
      @clients.send(HTTP::Client.new("hops.gm.th-koeln.de", tls: true))
    end
  end

  private def borrow_client(&block : HTTP::Client -> T) : T forall T
    client = @clients.receive
    ret = yield client
    @clients.send(client)
    ret
  end

  private def puts(message : String)
    LibC.printf("\r\033[1A#{message}\n\n")
  end

  private def begin_print_status(task_name : String, tasks_total : Int32, tasks_done : Int32)
    LibC.printf "#{task_name} [#{tasks_done}/#{tasks_total}]          \n"
  end

  private def print_status(task_name : String, tasks_total : Int32, tasks_done : Int32)
    LibC.printf "\r\033[1A#{task_name} [#{tasks_done}/#{tasks_total}]          \n"
  end

  def lecturers : Array(Lecturer)
    if lecturers = @lecturers
      return lecturers
    end
    lecturers, @degree_courses = self.fetch_lecturer_and_degree_course_abbrevations
    @lecturers = lecturers
    lecturers
  end

  def degree_courses : Array(DegreeCourse)
    if courses = @degree_courses
      return courses
    end
    @lecturers, degree_courses = self.fetch_lecturer_and_degree_course_abbrevations
    @degree_courses = degree_courses
    degree_courses
  end

  def timetable_entries : Array(TimetableEntry)
    if timetable_entries = @timetable_entries
      return timetable_entries
    end
    timetable_entries, @blocktable_entries = self.fetch_timetable
    @timetable_entries = timetable_entries
    timetable_entries
  end

  def blocktable_entries : Array(BlocktableEntry)
    if blocktable_entries = @blocktable_entries
      return blocktable_entries
    end
    @timetable_entries, blocktable_entries = self.fetch_timetable
    @blocktable_entries = blocktable_entries
    blocktable_entries
  end

  # Fetches and returns an array with all abbrevations from /hops/modules/common/abbreviation.php
  private def fetch_lecturer_and_degree_course_abbrevations : Tuple(Array(Lecturer), Array(DegreeCourse))
    begin_print_status "Fetching abbreviations", 1, 0

    abbrevations_res = borrow_client &.get("/hops/modules/common/abbreviation.php")
    raise RuntimeError.new("Fetching abbrevations returned HTTP status #{abbrevations_res.status_code}") unless abbrevations_res.success?
    abbrevations_doc = HTML5.parse(abbrevations_res.body)

    lecturers_abbrevations_container = nil
    courses_abbrevations_container = nil
    abbrevations_doc.css(".col-md-6").each do |doc|
      header = doc.css("h2")
      raise "Container has an invalid amount of h2 tags" unless header.size == 1
      case header.first.inner_text
      when "Dozentenkürzel" then lecturers_abbrevations_container = doc
      when "Studiengänge"   then courses_abbrevations_container = doc
      else
        raise RuntimeError.new("Unknown h2 tag with text #{header.first.inner_text}")
      end
    end
    raise "Lecturers abbrevations container not found" unless lecturers_abbrevations_container
    raise "Courses abbrevations container not found" unless courses_abbrevations_container

    lecturers_abbrevations = lecturers_abbrevations_container.css("dl").map do |row|
      key = row.css("dt")
      value = row.css("dd")
      raise "Missing abbrevation" unless key.size == 1
      raise "Missing value" unless value.size == 1
      Lecturer.new(key.first.inner_text.strip, value.first.inner_text.strip)
    end

    courses_abbrevations = courses_abbrevations_container.css("dl").map do |row|
      key = row.css("dt")
      value = row.css("dd")
      raise "Missing abbrevation" unless key.size == 1
      raise "Missing value" unless value.size == 1
      DegreeCourse.new(key.first.inner_text.strip, value.first.inner_text.strip)
    end

    print_status "Fetching abbreviations", 1, 1

    {lecturers_abbrevations, courses_abbrevations}
  end

  # Fetches and returns an array with all modules registered with HoPS
  private def fetch_modules : Array(Module)
    lecturers = self.lecturers

    begin_print_status "Fetching modules", -1, 0

    processed_ids = Set(String).new
    processed_ids_mutex = Mutex.new

    modulelist_res = borrow_client &.post("/hops/modules/modulelisting/ajaxmoduleoverview.php")
    raise RuntimeError.new("Fetching modulelisting returned HTTP status #{modulelist_res.status_code}") unless modulelist_res.success?
    ret = [] of Module
    return_channel = Channel(Module?).new(CLIENT_COUNT)

    total_parts = 0
    modulelist_res.body.split("<hr>", remove_empty: true) do |raw_module_desc|
      total_parts += 1
      spawn do
        module_desc = HTML5.parse(raw_module_desc)

        module_title = module_desc.css(".lecture-headline.lecture-lecturer2")
        raise RuntimeError.new("Lecture container contains multiple h1 nodes") unless module_title.size == 1
        module_name = module_title.first.inner_text.strip

        headline_links = module_desc.css(".lecture-lecturer > a")
        raise RuntimeError.new("Headline of fetched modulelisting contains multiple links") unless headline_links.size == 1
        module_link = headline_links.first["href"].val
        raise RuntimeError.new("Module link \"#{module_link}\" does not begin with module.php?mkz=") unless module_link.starts_with? "module.php?mkz="
        module_id = module_link.lchop("module.php?mkz=")

        already_processed = false
        processed_ids_mutex.synchronize do
          if processed_ids.includes?(module_id)
            already_processed = true
          else
            processed_ids << module_id
          end
        end
        if already_processed
          return_channel.send nil
          next
        end

        module_res = borrow_client &.get("/hops/modules/modulelisting/module.php?mkz=" + URI.encode_path(module_id))
        raise RuntimeError.new("Fetching module \"#{module_id}\" returned HTTP status #{module_res.status_code}") unless module_res.success?

        # XML is used here because most documents returned by HoPS are not HTML5-conformant, leading to weird behaviour
        module_doc = HTML5.parse(module_res.body.gsub("<p", "<tmp-tag").gsub("</p", "</tmp-tag"))

        module_print = module_doc.css("#module-print > tmp-tag")

        module_print_parts = {} of String => String
        module_print.each do |child|
          title_element = next_element_type_node?(child.first_child)
          next unless title_element
          child.remove_child(title_element)

          breakline_element = next_element_type_node?(child.first_child)
          next unless breakline_element
          child.remove_child(breakline_element)

          module_print_parts[title_element.inner_text.strip] = child.to_html(false).gsub("<tmp-tag", "<p").gsub("</tmp-tag", "</p")
        end

        unless module_print_parts.size == 20
          # Not my fault, this is pretty unparseable
          puts "WARN: Skipping misformatted module print for module id #{module_id}"
          return_channel.send nil
          next
        end

        unknown_lecturers = [] of String
        lecturer_ids = [] of String
        module_print_parts["Lehrende(r):"].strip.split(',').each do |prof|
          dot_index = prof.rindex('.') || -2

          if lecturer = lecturers.find { |lecturer| lecturer.name == prof[(dot_index + 2)..]?.try &.strip }
            lecturer_ids << lecturer.id
          else
            unknown_lecturers << prof
          end
        end

        unknown_responsibles = [] of String
        responsibles_ids = [] of String
        module_print_parts["Modulverantwortliche(r):"].strip.split(',').each do |prof|
          dot_index = prof.rindex('.') || -2

          if lecturer = lecturers.find { |lecturer| lecturer.name == prof[(dot_index + 2)..]?.try &.strip }
            responsibles_ids << lecturer.id
          else
            unknown_responsibles << prof
          end
        end

        lectures_html = HTML5.parse(module_print_parts["Lehrveranstaltung(en):"])
        lecture_ids = lectures_html.css("a").map &.["href"].val.lchop("../lecturelisting/lecture.php?fkz=")

        accreditation_date, last_change = module_print_parts["Akkreditierungsdatum:"].split("<b>Letzte Änderung: </b>").map do |date|
          Time.parse_local(date.strip, "%d.%m.%y")
        end

        return_channel.send Module.new(
          id: module_id,
          name: module_name,
          lecture_ids: lecture_ids,
          accreditation_date: accreditation_date,
          last_change: last_change,
          responsibles_ids: responsibles_ids,
          unknown_responsibles: unknown_responsibles,
          lecturer_ids: lecturer_ids,
          unknown_lecturers: unknown_lecturers,
          semester_raw_html: module_print_parts["Semester:"],
          language_raw_html: module_print_parts["Sprache:"],
          course_degree_assignment_raw_html: module_print_parts["Studiengang/Zuordnung - Curriculum/Semester:"],
          semester_hours_raw_html: module_print_parts["Semesterwochenstunden:"],
          group_size_raw_html: module_print_parts["Gruppengröße Praktikum:"],
          teaching_format_raw_html: module_print_parts["Lehrform:"],
          workload_raw_html: module_print_parts["Arbeitsaufwand:"],
          credits_raw_html: module_print_parts["Credits:"],
          prerequisites_raw_html: module_print_parts["Voraussetzungen:"],
          goals_raw_html: module_print_parts["Lernziele/Kompetenzen:"],
          content_raw_html: module_print_parts["Inhalt:"],
          work_raw_html: module_print_parts["Leistungen:"],
          media_formats_raw_html: module_print_parts["Medienformen:"],
          literature_raw_html: module_print_parts["Literatur:"],
          focuses_raw_html: module_print_parts["Schwerpunkte:"],
          other_information_raw_html: module_print_parts["Sonstige Informationen:"]
        )
      end
    end

    print_status "Fetching modules", total_parts, 0
    total_parts.times do |i|
      tmp = return_channel.receive
      ret << tmp if tmp
      print_status "Fetching modules", total_parts, i + 1
    end

    ret
  end

  # Fetches and returns an array with all lectures registered with HoPS
  private def fetch_lectures : Array(Lecture)
    lecturers = self.lecturers

    begin_print_status "Fetching lectures", -1, 0

    processed_ids = Set(String).new
    processed_ids_mutex = Mutex.new

    lecturelist_res = borrow_client &.post("/hops/modules/lecturelisting/ajaxlectureoverview.php")
    raise RuntimeError.new("Fetching lecturelisting returned HTTP status #{lecturelist_res.status_code}") unless lecturelist_res.success?
    ret = [] of Lecture
    return_channel = Channel(Lecture?).new(CLIENT_COUNT)

    total_parts = 0
    lecturelist_res.body.split("<hr>", remove_empty: true) do |raw_lecture_desc|
      total_parts += 1
      spawn do
        lecture_desc = HTML5.parse(raw_lecture_desc)

        lecture_title = lecture_desc.css(".lecture-headline.lecture-lecturer2")
        raise RuntimeError.new("Lecture container contains multiple h1 nodes") unless lecture_title.size == 1
        name_abbreviation = lecture_title.first.inner_text.strip
        bracket_open_index = name_abbreviation.rindex(" (")
        raise RuntimeError.new("Lecture name is not in format \"name (abbreviation)\"") unless name_abbreviation.ends_with?(')') && bracket_open_index

        headline_links = lecture_desc.css(".lecture-lecturer > a")
        raise RuntimeError.new("Headline of fetched lecturelisting contains multiple links") unless headline_links.size == 1
        lecture_link = headline_links.first["href"].val
        raise RuntimeError.new("Lecture link \"#{lecture_link}\" does not begin with lecture.php?fkz=") unless lecture_link.starts_with? "lecture.php?fkz="
        lecture_id = lecture_link.lchop("lecture.php?fkz=")

        already_processed = false
        processed_ids_mutex.synchronize do
          if processed_ids.includes?(lecture_id)
            already_processed = true
          else
            processed_ids << lecture_id
          end
        end
        if already_processed
          return_channel.send nil
          next
        end

        lecture_res = borrow_client &.get("/hops/modules/lecturelisting/lecture.php?fkz=" + URI.encode_path(lecture_id))
        raise RuntimeError.new("Fetching lecture \"#{lecture_id}\" returned HTTP status #{lecture_res.status_code}") unless lecture_res.success?

        # XML is used here because most documents returned by HoPS are not HTML5-conformant, leading to weird behaviour
        lecture = HTML5.parse(lecture_res.body.gsub("<p", "<tmp-tag").gsub("</p", "</tmp-tag"))

        lecture_print = lecture.css("#lecture-print > tmp-tag")
        unless lecture_print.size == 10
          # Not my fault, this is pretty unparseable
          # See https://hops.gm.th-koeln.de/hops/modules/lecturelisting/lecture.php?fkz=MA1B
          puts "WARN: Skipping misformatted lecture print for lecture id #{lecture_id}"
          return_channel.send nil
          next
        end

        lecture_print_parts = lecture_print.to_h do |child|
          title_element = next_element_type_node?(child.first_child)
          raise "No title element found" unless title_element
          child.remove_child(title_element)

          breakline_element = next_element_type_node?(child.first_child)
          raise "No breakline element found" unless breakline_element
          child.remove_child(breakline_element)

          {title_element.inner_text.strip, child.to_html(false).gsub("<tmp-tag", "<p").gsub("</tmp-tag", "</p")}
        end

        lecture_abbreviation = name_abbreviation[(bracket_open_index + 2)..-2]
        lecture_name = name_abbreviation[0..(bracket_open_index - 1)]

        unknown_lecturers = [] of String
        lecturer_ids = [] of String
        lecture_print_parts["Dozent/in:"].strip.split(',').each do |prof|
          dot_index = prof.rindex('.') || -2

          if lecturer = lecturers.find { |lecturer| lecturer.name == prof[(dot_index + 2)..]?.try &.strip }
            lecturer_ids << lecturer.id
          else
            unknown_lecturers << prof
          end
        end

        semesters = lecture_print_parts["Semester:"].split(',').map &.to_i

        modules_html = HTML5.parse(lecture_print_parts["Modulzuordnung (laut Prüfungsordnung):"])
        module_ids = modules_html.css("a").map &.["href"].val.lchop("../modulelisting/module.php?mkz=")

        return_channel.send Lecture.new(
          id: lecture_id,
          name: lecture_name,
          abbreviation: lecture_abbreviation,
          lecturer_ids: lecturer_ids,
          unknown_lecturers: unknown_lecturers,
          semesters: semesters,
          module_ids: module_ids,
          content_raw_html: lecture_print_parts["Inhalt:"],
          literature_raw_html: lecture_print_parts["Literatur:"],
          prerequisites_raw_html: lecture_print_parts["Voraussetzungen:"],
          goals_raw_html: lecture_print_parts["Lernziele/Kompetenzen:"],
          media_formats_raw_html: lecture_print_parts["Medienformen:"],
          curriculum_occurences_raw_html: lecture_print_parts["Im Curriculum der Studiengänge:"]
        )
      end
    end

    print_status "Fetching lectures", total_parts, 0
    total_parts.times do |i|
      tmp = return_channel.receive
      ret << tmp if tmp
      print_status "Fetching lectures", total_parts, i + 1
    end

    ret
  end

  private SEMESTER_COUNT = 8

  # Fetches and returns an array with all lectures registered with HoPS
  private def fetch_timetable : Tuple(Array(TimetableEntry), Array(BlocktableEntry))
    finished_timetable_entries = [] of TimetableEntry
    finished_blocktable_entries = [] of BlocktableEntry
    degree_courses = self.degree_courses

    begin_print_status "Fetching timetable", SEMESTER_COUNT, 0

    finished_timetable_channel = Channel(Array(TimetableEntry)).new(SEMESTER_COUNT)
    finished_blocktable_channel = Channel(Array(BlocktableEntry)).new(SEMESTER_COUNT)

    (1..SEMESTER_COUNT).each do |semester|
      spawn do
        timetable_res = borrow_client &.post("/hops/modules/timetable/ajaxgetresult.php", form: "semester=#{semester}")
        raise RuntimeError.new("Fetching timetable returned HTTP status #{timetable_res.status_code}") unless timetable_res.success?
        timetable_doc = HTML5.parse(timetable_res.body)

        tbodies = timetable_doc.css("#timetable > tbody")
        raise RuntimeError.new unless tbodies.size == 1
        current_timeslot = tbodies.first.first_child

        finished_entries = [] of TimetableEntry
        previous_entries = [] of TimetableEntry
        current_entries = [] of TimetableEntry

        while current_timeslot
          next current_timeslot = current_timeslot.next_sibling unless current_timeslot.type.element?

          time_of_day_object = current_timeslot.first_child
          raise RuntimeError.new unless time_of_day_object
          time_of_day_object = next_element_type_node(time_of_day_object)
          time_of_day_raw = time_of_day_object.inner_text.strip
          hour, minute = time_of_day_raw.split('.').map &.to_i

          previous_entries.map! do |entry|
            entry.copy_with(hour_end: hour, minute_end: minute)
          end

          current_day_object = time_of_day_object
          (1..5).each do |day_of_week_index|
            day_of_week = Time::DayOfWeek.from_value(day_of_week_index)
            current_day_object = next_element_type_node?(current_day_object.next_sibling)
            raise RuntimeError.new unless current_day_object
            current_anchor = current_day_object.first_child

            while current_anchor = next_element_type_node?(current_anchor)
              lecture_ref = current_anchor["href"].val
              raise RuntimeError.new unless lecture_ref.starts_with?("../lecturelisting/lecture.php?fkz=")
              lecture_id = lecture_ref.lchop("../lecturelisting/lecture.php?fkz=")
              timetable_entry_type_tag = current_anchor.first_child
              raise RuntimeError.new("Timetable entry type tag not found") unless timetable_entry_type_tag
              timetable_entry_type_raw = timetable_entry_type_tag["class"].val
              timetable_entry_type = TimetableEntryType.from_raw(timetable_entry_type_raw)
              timetable_entry_description = timetable_entry_type_tag.inner_text.strip

              room_id_index = timetable_entry_description.rindex(' ')
              raise RuntimeError.new("Could not find room id index") unless room_id_index
              room_id = timetable_entry_description[(room_id_index + 1)..]

              desc_parts = timetable_entry_description.split(' ')
              current_entry = TimetableEntry.new(
                lecture_id: lecture_id,
                day_of_week: day_of_week,
                room_id: room_id,
                type: timetable_entry_type,
                hour_start: hour,
                hour_end: hour,
                minute_start: minute,
                minute_end: minute,
                semester: semester,
                degree_course_ids: degree_courses.select { |course| desc_parts.includes?(course.id) }.map &.id,
              )

              previous_index = previous_entries.index do |entry|
                entry.lecture_id == lecture_id &&
                  entry.day_of_week == day_of_week &&
                  entry.room_id == room_id &&
                  entry.type == timetable_entry_type
              end

              if previous_index
                previous_entry = previous_entries[previous_index]
                current_entry = current_entry.copy_with(
                  hour_start: previous_entry.hour_start,
                  minute_start: previous_entry.minute_start
                )
                previous_entries.delete_at(previous_index)
              end

              current_entries << current_entry

              current_anchor = current_anchor.next_sibling
            end
          end

          previous_entries.each { |entry| finished_entries << entry }
          previous_entries = current_entries
          current_entries = [] of TimetableEntry

          current_timeslot = current_timeslot.next_sibling
        end

        finished_timetable_channel.send(finished_entries)

        raise "List of unfinished entries not empty" unless previous_entries.empty?

        blocktable_entries = [] of BlocktableEntry
        blocktable_entries_raw = timetable_doc.css("#blocktable > tbody > tr")
        blocktable_entries_raw.each do |entry|
          values = entry.css("td").map &.inner_text

          blocktable_entries << BlocktableEntry.new(
            time_raw: values[0],
            lecture_id_raw: values[1],
            description: values[2],
            lecturer_raw: values[3],
            room: values[4],
            degree_course_id: values[5],
            semester: semester
          )
        end

        finished_blocktable_channel.send(blocktable_entries)
      end
    end

    SEMESTER_COUNT.times do |i|
      finished_timetable_channel.receive.each do |timetable_entry|
        finished_timetable_entries << timetable_entry
      end

      finished_blocktable_channel.receive.each do |blocktable_entry|
        finished_blocktable_entries << blocktable_entry
      end

      print_status "Fetching timetable", SEMESTER_COUNT, i + 1
    end

    {finished_timetable_entries, finished_blocktable_entries}
  end

  private def next_element_type_node(node : HTML5::Node) : HTML5::Node
    until node.type.element?
      node = node.next_sibling
      raise RuntimeError.new("No next sibling available") unless node
    end
    node
  end

  private def next_element_type_node?(node : HTML5::Node?) : HTML5::Node?
    return node unless node
    until node.type.element?
      node_ret = node.next_sibling
      return nil unless node_ret
      node = node_ret
    end
    node
  end
end
