require "http/client"
require "html5"
require "json"
require "./hops_scraper/*"

# TODO: Write documentation for `HoPSScraper`
module HoPSScraper
  VERSION = "0.1.0"

  enum TimetableEntryType : UInt8
    Lecture
    Practicum
    Practice
    Seminar
    Tutorium

    def self.from_raw(string : String) : self
      case string
      when "V"  then Lecture
      when "P"  then Practicum
      when "UE" then Practice
      when "S"  then Seminar
      when "T"  then Tutorium
      else
        raise ArgumentError.new("Unknown value: #{string}")
      end
    end
  end

  record TimetableEntry,
    lecture_id : String,
    day_of_week : Time::DayOfWeek,
    room_id : String,
    type : TimetableEntryType,
    hour_start : Int32,
    hour_end : Int32,
    minute_start : Int32,
    minute_end : Int32,
    degree_course_ids : Array(String),
    semester : Int32 do
    include JSON::Serializable
  end

  record BlocktableEntry,
    time_raw : String,
    lecture_id_raw : String,
    description : String,
    lecturer_raw : String,
    room : String,
    degree_course_id : String,
    semester : Int32 do
    include JSON::Serializable
  end

  record Lecturer, id : String, name : String do
    include JSON::Serializable
  end

  record DegreeCourse, id : String, name : String do
    include JSON::Serializable
  end

  record Lecture,
    id : String,
    name : String,
    abbreviation : String,
    lecturer_ids : Array(String),
    unknown_lecturers : Array(String),
    semesters : Array(Int32),
    module_ids : Array(String),
    content_raw_html : String,
    literature_raw_html : String,
    prerequisites_raw_html : String,
    goals_raw_html : String,
    media_formats_raw_html : String,
    curriculum_occurences_raw_html : String do
    include JSON::Serializable
  end

  record Module,
    id : String,
    name : String,
    lecture_ids : Array(String),
    accreditation_date : Time,
    last_change : Time,
    responsibles_ids : Array(String),
    unknown_responsibles : Array(String),
    lecturer_ids : Array(String),
    unknown_lecturers : Array(String),
    semester_raw_html : String,
    language_raw_html : String,
    course_degree_assignment_raw_html : String,
    semester_hours_raw_html : String,
    group_size_raw_html : String,
    teaching_format_raw_html : String,
    workload_raw_html : String,
    credits_raw_html : String,
    prerequisites_raw_html : String,
    goals_raw_html : String,
    content_raw_html : String,
    work_raw_html : String,
    media_formats_raw_html : String,
    literature_raw_html : String,
    focuses_raw_html : String,
    other_information_raw_html : String do
    include JSON::Serializable
  end
end

grabber = HoPSScraper::Grabber.new
File.write("output.json", {
  lecturers:          grabber.lecturers,
  degree_courses:     grabber.degree_courses,
  lectures:           grabber.lectures,
  modules:            grabber.modules,
  timetable_entries:  grabber.timetable_entries,
  blocktable_entries: grabber.blocktable_entries,
}.to_json)
