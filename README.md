# hops_scraper

A scraper for HoPS.

[https://hops.gm.th-koeln.de](HoPS) ("**Ho**chschul**P**lanungs**S**ystem") is a system used by the [TH Köln](https://www.th-koeln.de) (University of Applied Sciences Cologne) for distributing students' timetables, lecture information, room information and module information as well as booking rooms.

The goal of this project is to provide the information distributed via HoPS in a more convient manner (ex. modern app, iCal calendars).

## Installation

Install the shards using `shards` and compile the program with `crystal build --release src/hops_scraper.cr`.

## Usage

The program currently accepts no arguments. Just execute the binary and after a few minutes, a file called `output.json` will be created in your current directory.

## Development

TODO: Write development instructions here

## Contributing

1. Fork it (<https://gitlab.com/BlobCodes/hops-scraper/-/forks/new>)
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create a new Pull Request

## Contributors

- [David Keller](https://gitlab.com/BlobCodes) - creator and maintainer
